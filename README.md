# T-Clima

Aplicación para informe del clima de *argentina*.

## Getting Started

Estas instrucciones le proporcionarán una copia del proyecto en funcionamiento en su máquina local para fines de desarrollo y prueba.

### Pre-requisitos

Qué cosas necesita para instalar la aplicación y cómo instalarlas

* [Instalar Git](/git-readme.md)


### Instalación

Primero realizar la clonación del proyecto en una carpeta destinada al desarrollo de las aplicaciones.

En sua consola ubiquese dentro de la carpeta destino y ejecute el siguiente comando en su consola
```
git clone https://gitlab.com/cristian.mari.dev/telecom.git
```




## Comandos de la aplicación

| Comando  |  Descripción |  
|---|---|
| dev  |  Inicializa la aplicación en modo desarrollo tendrá actualización en vivo y funcionalidades que le ayudaran con sus tareas.  |  
|  build |  Inicializa la construcción del empaquetado de su sitio web optimizado para ambitos productivos. | 
|  test |  Inicializa las pruebas unitarias a la aplicación de react. | 


### Ejemplo del uso de los comandos:

```
npm run dev (o) yarn dev << Si tiene usa yarn >>
npm run build (o) yarn build  << Si tiene usa yarn >>
```

