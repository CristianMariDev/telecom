/**
 * @desc Polyfill de babel.
 */
require("@babel/polyfill");

/**
 * @desc Dependencias
 */
const { configure } = require( 'enzyme' );
const Adapter = require( 'enzyme-adapter-react-16' );


/**
 * @desc Configuramos el adaptador.
 */
configure({ adapter: new Adapter() })

/**
 * @desc Mock's
 */
window.scrollTo = jest.fn();