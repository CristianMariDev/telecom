/**
 * @desc Convertimos de Kelvin a Celsius
 * 
 * @param { Number } 
 * 
 * @return { Number }
 */
export const KelvinToCelsius = ( kelvin ) => {
    return parseFloat( kelvin - 273.15).toFixed(1);
};