/**
 * @desc Dependencias
 */
import React from 'react';

/**
 * @desc Componentes estilisados
 */
import { Overlay, Content, Title, Spinner } from "./style";

/**
 * @desc Componente del loader
 * 
 * @param { Object } props
 * 
 * @return { HTMLDivElement }
 */
export default ({ ...props }) => {

    return (
        <Overlay>
            <Content justify={ "center" } alignItems={ "center" } alignContent={ "center" }>
                <Title>Cargando, aguarde por favor...</Title>
                <Spinner /> 
            </Content>
        </Overlay>
    );

};