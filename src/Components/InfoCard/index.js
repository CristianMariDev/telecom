/**
 * @desc Dependencias
 */
import React from 'react';

/**
 * @desc Componentes estilisados
 */
import { InfoCard, Title, Content } from "./style";

/**
 * @desc Componente de carta para información del clima
 * 
 * @param { Object } props
 * 
 * @return { HTMLDivElement }
 */
export default ({ ...props }) => {

    // Alias de las propiedades
    const {
        // Titulo de la carta
        title,
        // Contenido de la carta.
        content
    } = props;

    return (
        <InfoCard item={ true } xs={ 12 }>
            <Title>{ title }</Title>
            <Content>{ content }</Content>
        </InfoCard>
    );

};