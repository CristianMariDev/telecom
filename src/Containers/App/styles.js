/**
 * @desc Dependencias
 */
import styled from "styled-components";

/**
 * @desc Material Design
 */
import { Grid } from "@material-ui/core";

/**
 * @desc Contenedor principal
 */
export const Main = styled.main`
    max-width:1200px;
    margin:0px auto;
    border-radius:4px;
`;

export const Wrapper = styled(Grid)`
    @media (max-width: 768px) {
        &{
            flex-direction:column-reverse;
        }
    }    
`;