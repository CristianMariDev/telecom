/**
 * @desc Dependencias
 */
import React from "react";
import { shallow } from "enzyme";
import 'jest-styled-components'

/**
 * @desc Contenedor
 */
import AppContainer from "./App.jsx";

/**
 * @desc Suite de test's
 */
describe("[Containers.App]", () => {
    
	/**
	 * @desc Testeamos el snapshot.
	 */
    it("[Containers.App] Verificamos el renderizado del contenedor App", () => {

        // Renderizamos el contenedor.
        const rendered = shallow( <AppContainer /> );

        // Comparamos el snapshot
        expect( rendered ).toMatchSnapshot();

    });

	/**
	 * @desc Verificamos si la busqueda de una ciudad funciona correctamente.
	 */
    it('[Containers.AppUI] Verificamos el metodo .changeCity() - Con datos reales', async () => {

        // Renderizamos
        const rendered = shallow( <AppContainer /> ),
              instance= rendered.instance();

        // Llamamos al cambio de ciudad.
        const result = await instance.changeCity( "Buenos Aires" );

        // Verificamos el resultado
        expect( result ).toBeTruthy();

    });
    
	/**
	 * @desc Verificamos si la busqueda de una ciudad funciona correctamente.
	 */
    it('[Containers.AppUI] Verificamos el metodo .changeCity() - Con datos erroneos.', async () => {

        // Renderizamos
        const rendered = shallow( <AppContainer /> ),
              instance= rendered.instance();

        // Errores
        const errors = [null, 1, function(){}, {}, []];

        // Recolectamos los errores.
        errors.forEach( async data => {
            expect( await instance.changeCity( data ) ) .toEqual( false );
        });

    });

	/**
	 * @desc Verificamos si obtiene los datos del cliente.
	 */
    it('[Containers.AppUI] Verificamos el metodo .getClientData() - Con datos reales', async () => {

        // Renderizamos
        const rendered = shallow( <AppContainer /> ),
              instance= rendered.instance();

        // Llamamos al cambio de ciudad.
        await instance.getClientData( function( client, currentCity ){

            expect( client ).toMatchObject({
                "country"	: expect.any( String ),
                "province"	: expect.any( String ),
                "city"		: expect.any( String ),
                "cp"		: expect.any( String ),
                "coords"	: {
                    "lat"	: expect.any( Number ),
                    "lng"	: expect.any( Number )
                },
                "ip"		: expect.any( String )
            })

            expect( currentCity ).toEqual( expect.any( String ) );

        } );

    });

	/**
	 * @desc Verificamos si obtiene los datos del cliente.
	 */
    it('[Containers.AppUI] Verificamos el metodo .getClientData() - Con datos erroneos', async () => {

        // Renderizamos
        const rendered = shallow( <AppContainer /> ),
              instance= rendered.instance();

        // Errores
        const errors = [null, 1, function(){}, {}, []];

        // Recolectamos los errores.
        errors.forEach( async data => {
            expect( await instance.getClientData( data ) ).toBeTruthy();
        });        

    });

	/**
	 * @desc Verificamos si obtiene los datos del cliente.
	 */
    it('[Containers.AppUI] Verificamos el metodo .getWeatherCurrent() - Con datos reales', async () => {

        // Renderizamos
        const rendered = shallow( <AppContainer /> ),
              instance= rendered.instance();

        // Llamamos al cambio de ciudad.
        expect( await instance.getWeatherCurrent( "Buenos Aires" ) ).toBeTruthy();

    });

	/**
	 * @desc Verificamos si obtiene los datos del cliente.
	 */
    it('[Containers.AppUI] Verificamos el metodo .getWeatherCurrent() - Con datos erroneos', async () => {

        // Renderizamos
        const rendered = shallow( <AppContainer /> ),
              instance = rendered.instance();

        // Errores
        const errors = [null, 1, function(){}, {}, []];

        // Recolectamos los errores.
        errors.forEach( async data => {
            expect( await instance.getWeatherCurrent( data ) ).toBeFalsy();
        });        

    });

	/**
	 * @desc Verificamos si obtiene los datos del cliente.
	 */
    it('[Containers.AppUI] Verificamos el metodo .getWeatherOfOtherCitys() - Con datos reales', async () => {

        // Renderizamos
        const rendered = shallow( <AppContainer /> ),
              instance= rendered.instance();

        // Datos del cliente
        const client = {
            "coords":{
                "lat": -34.7474,
                "lng": -58.3958
            }
        };

        // Llamamos al cambio de ciudad.
        expect( await instance.getWeatherOfOtherCitys( client ) ).toBeTruthy();

    });

	/**
	 * @desc Verificamos si obtiene los datos del cliente.
	 */
    it('[Containers.AppUI] Verificamos el metodo .getWeatherOfOtherCitys() - Con datos erroneos', async () => {

        // Renderizamos
        const rendered = shallow( <AppContainer /> ),
              instance = rendered.instance();

        // Errores
        const errors = [null, 1, function(){}, {}, []];

        // Recolectamos los errores.
        errors.forEach( async data => {
            expect( await instance.getWeatherOfOtherCitys( data ) ).toBeFalsy();
        });        

    });

	/**
	 * @desc Verificamos si obtiene los datos del cliente.
	 */
    it('[Containers.AppUI] Verificamos el metodo .getForecast() - Con datos reales', async () => {

        // Renderizamos
        const rendered = shallow( <AppContainer /> ),
              instance= rendered.instance();

        // Llamamos al cambio de ciudad.
        expect( await instance.getForecast( "Buenos aires" ) ).toBeTruthy();

    });

	/**
	 * @desc Verificamos si obtiene los datos del cliente.
	 */
    it('[Containers.AppUI] Verificamos el metodo .getForecast() - Con datos erroneos', async () => {

        // Renderizamos
        const rendered = shallow( <AppContainer /> ),
              instance = rendered.instance();

        // Errores
        const errors = [null, 1, function(){}, {}, []];

        // Recolectamos los errores.
        errors.forEach( async data => {
            expect( await instance.getForecast( data ) ).toBeFalsy();
        });        

    });

	/**
	 * @desc Verificamos si obtiene los datos del cliente.
	 */
    it('[Containers.AppUI] Verificamos el metodo .generateSchemeForecast() - Con datos reales', async () => {

        // Renderizamos
        const rendered = shallow( <AppContainer /> ),
              instance= rendered.instance();

        // Datos
        const data = [{
            "dt_txt": 1560281377,
            "main":{
                "temp_min": 15,
                "temp_max": 20
            },
            "weather": [{
                "description": "Nublado",
            }]
        }];

        // Llamamos al cambio de ciudad.
        expect( await instance.generateSchemeForecast( data ) ).toEqual(
            expect.arrayContaining([expect.objectContaining({
                "status": expect.any( String ),
                "date": expect.any( String ),
                "temp": {
                    "min": expect.any( String ),
                    "max": expect.any( String )
                }
            })])
          )
      
    });

	/**
	 * @desc Verificamos si obtiene los datos del cliente.
	 */
    it('[Containers.AppUI] Verificamos el metodo .generateSchemeForecast() - Con datos erroneos', async () => {

        // Renderizamos
        const rendered = shallow( <AppContainer /> ),
              instance= rendered.instance();
              
        // Errores
        const errors = [null, 1, function(){}, {}, []];

        // Recolectamos los errores.
        errors.forEach( async data => {
            expect( await instance.generateSchemeForecast( data, data ) ).toEqual([]);
        });        

      
    });
    
	/**
	 * @desc Verificamos si obtiene los datos del cliente.
	 */
    it('[Containers.AppUI] Verificamos el metodo .filterForecastByHour() - Con datos reales', async () => {

        // Renderizamos
        const rendered = shallow( <AppContainer /> ),
              instance= rendered.instance();

        // Datos
        const data = new Array(30).fill({
            "status": true
        });

        // Llamamos al cambio de ciudad.
        expect( await instance.filterForecastByHour( data, 8 ) ).toEqual(
            expect.arrayContaining([
                expect.objectContaining({
                    "status": expect.any( Boolean )
                })
            ])
          )
      
    });

	/**
	 * @desc Verificamos si obtiene los datos del cliente.
	 */
    it('[Containers.AppUI] Verificamos el metodo .filterForecastByHour() - Con datos erroneos', async () => {

        // Renderizamos
        const rendered = shallow( <AppContainer /> ),
              instance= rendered.instance();
              
        // Errores
        const errors = [null, 1, function(){}, {}, []];

        // Recolectamos los errores.
        errors.forEach( async data => {
            expect( await instance.generateSchemeForecast( data, data ) ).toEqual([]);
        });        

      
    });

});