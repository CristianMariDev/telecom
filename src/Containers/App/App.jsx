/**
 * @desc Dependencias
 */
import React from 'react';

/**
 * @desc Controlador
 */
import AppUI from "./AppUI.js";

/**
 * @desc Paginas
 */
import SectionParallax from './Sections/SectionParallax/index.js';
import SectionFutureWeather from './Sections/SectionFutureWeather/index.js';
import SectionCitys from './Sections/SectionCitys/index.js';
/**
 * @desc MaterialDesign
 */
import { Grid } from "@material-ui/core"

/**
 * @desc Componentes estilisados
 */
import { Main, Wrapper } from "./styles.js";

/**
 * @desc Componentes
 */
import InfoCard from "../../Components/InfoCard";
import Loading from "../../Components/Loading";

/**
 * @desc Contenedor de la pantalla de la aplicación
 */
class App extends AppUI{

	/**
	 * @desc Renderiza el contenedor.
	 * 
	 * @return { HTMLSectionElement }
	 */
	render(){

		// Alias de las propiedades
		const { 
			
			// Datos del cliente.
			client = null,

			// Clima actual
			currentWeather = null,

			// Pronostico del tiempo
			forecast = null,

			// Ciudades cercanas
			citysNearby = null,

			// Ciudad Actual.
			currentCity = null,

			// Precarga
			loading = true
		
		} = this.state;

		return (
			<Main>

				{ loading && <Loading /> }

				{ /* Cabecera */ }
				<SectionParallax client={ client } currentWeather={ currentWeather } currentCity={ currentCity }>

					{ /* Velocidad del viento */ }
					<Grid container={ true } wrap={ "wrap" } justify={"space-evenly"}>

						{ /* Amanecer */ }
						<InfoCard title={"Amanecer"} content={ currentWeather ? new Date(currentWeather.sunrise * 1000).toLocaleTimeString() : '' } />

						{ /* Velocidad del viento */ }
						<InfoCard title={"Velocidad del viento"} content={ currentWeather?.wind+"km" ?? "" } />

						{ /* Humedad */ }
						<InfoCard title={"Humedad"} content={ currentWeather?.humidity+"%" ?? '' } />

						{ /* Atardecer */ }
						<InfoCard title={"Atardecer"} content={ currentWeather ? new Date(currentWeather.sunset * 1000).toLocaleTimeString() : '' } />

					</Grid>

				</SectionParallax>

				{ /* Contenedor */ }
				<Wrapper container={ true } alignItems={ "flex-start" }>

					{ /* Otras ciudades */ }
					<SectionCitys data={ citysNearby } onSelect={ name => this.changeCity( name ) } />

					{ /* Clima de los proximos dias */ }
					<SectionFutureWeather data={ forecast } />

				</Wrapper>

			</Main>
		);

	}

}

export default App;
