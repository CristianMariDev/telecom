/**
 * @desc Dependencias
 */
import React from 'react';

/**
 * @desc Componentes estilisados
 */
import { Citys, City, Title, Name, Temp } from "./styles";

/**
 * @desc Componentes
 * 
 * @param { Object } props
 * 
 * @return { HTMLSection }
 */
export default ({ ...props }) => {

    // Alias de las propiedades
    const {
        // Datos de las ciudades.
        data = null,
        // Evento de selección
        onSelect = () => {}
    } = props;

    return (
        <Citys container={ true } item={ true } md={ 6 } xs={ 12 } wrap={ "wrap" } justify={ "space-evenly" }>
            
            <Title>Otras Ciudades</Title>

            { /* Ciudad */ }
            {
                Array.isArray( data ) && data.map( city => (
                    <City onClick={ () => onSelect( city?.name ) }>
                        <Name>{ city?.name }</Name>
                        <Temp>{ city?.temp }</Temp>
                    </City>
                ) )
            }

        </Citys>
    );

};