/**
 * @desc Dependencias
 */
import styled from "styled-components";

/**
 * @desc Material Design
 */
import { Grid } from "@material-ui/core";

/**
 * @desc Estilos
 */
import ImageBsAs from "../../../../Assets/img/citys/bs_as_space.jpg";

/**
 * @desc Linea del tiempo
 */
export const Citys = styled( Grid )`
    @media (max-width: 768px) {
        &{
            padding-top:40px;
        }
    }    
`;

/**
 * @desc Dia
 */
export const City = styled.div`
    display:inline-flex;
    background:url(${ ImageBsAs });
    background-size:250%;
    flex-basis:-webkit-calc(50% - 50px);
    margin:10px 5px;
    padding:10px;
    border:1px solid #d6d8da;
    border-radius:4px;
    box-shadow:2px 0px 2px -2px rgba(0,0,0,.3);
    flex-wrap: wrap;
    @media (max-width: 768px) {
        &{
            padding:20px 10px;
        }
    }    
    @media (max-width: 425px) {
        &{
            flex-basis:-webkit-calc(100% - 50px);
        }
    }    
`;

/**
 * @desc Titulo
 */
export const Title = styled.h2`
    color:#333;
    width:100%;
    font-family:'IBM Plex Sans', sans-serif;
    margin:0;
    margin-bottom:20px;
    text-align:center;
`;

/**
 * @desc Nombre de la ciudad
 */
export const Name = styled.h3`
    color:#fff;
    width:100%;
    font-family:'IBM Plex Sans', sans-serif;
    font-size:1rem;
    text-align:center;
`;

/**
 * @desc Temperatura de la ciudad
 */
export const Temp = styled.h3`
    color:#fff;
    width:100%;
    font-family:'IBM Plex Sans', sans-serif;
    font-size:1.5rem;
    margin: 0;
    margin-top:20px;
    text-shadow:2px 0px 2px rgba(0,0,0,.5);
    text-align:right;
`;
