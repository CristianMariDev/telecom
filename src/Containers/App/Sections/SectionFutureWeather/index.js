/**
 * @desc Dependencias
 */
import React from 'react';

/**
 * @desc Componentes estilisados
 */
import { Timeline, DateDay, Day, Title, Forecast, Link, Temp, Min, Max } from "./styles";

/**
 * @desc Componentes
 * 
 * @param { Object } props
 * 
 * @return { HTMLSection }
 */
export default ({ ...props }) => {

    // Alias de las propiedades
    const { data = null } = props;

    return (
        <Timeline container={ true } item={ true } md={ 6 } wrap={ "wrap" } justify={ "center" }>

            <Title>Proximos Días</Title>

            {
                Array.isArray( data ) && data.map(( forecast, index ) => (
                    <Day item={ true } md={ 12 }>
                        <DateDay>{ forecast.date }</DateDay>
                        <Forecast>{ forecast.status }</Forecast>
                        <Temp container={ true } wrap={ "nowrap" }>
                            <Min>Minima de: { forecast?.temp?.min } c</Min>
                            <Max>Maxima de: { forecast?.temp?.max } c</Max>
                        </Temp>
                    </Day>
                ))

            }
            
        </Timeline>
    );

};