/**
 * @desc Dependencias
 */
import styled, { css } from "styled-components";

/**
 * @desc Iconos
 */
import DayBackground from "../../../../Assets/img/weather/day.png";
import NightBackground from "../../../../Assets/img/weather/night.png";

/**
 * @desc Imagenes por clima
 */
const turn = {
	day : DayBackground,
	night: NightBackground
}

/**
 * @desc Contenedor principal
 */
export const Background = styled.figure`
	${
		props => turn[props["data-turn"]] && css`
			background-image:url(${turn[props["data-turn"]]});
		`
	}
	background-size:cover;
	background-position:top ;
	background-repeat:no-repeat;
	height:45vh;
	margin:0;
	margin-bottom:15vh;
	position:relative;
	overflow:visible;
	box-shadow:2px 1px 8px 1px rgba(0,0,0,0.3);
	border-radius:4px;
	@media (max-width: 425px) {
        &{
			padding-bottom:100px;
			margin-bottom:120px;
        }
    }    
`;


/**
 * @desc Contenedor principal
 */
export const DayDate = styled.h1`
	color:white;
	font-family:'IBM Plex Sans', sans-serif;
	font-size:1.5rem;
	text-shadow:1px 1px 1px rgba(0,0,0,0.5);
	margin:2vh;
	@media (max-width: 425px) {
        &{
			width: 100%;
			text-align:center;
			font-size:1rem;
        }
    }    
`;

/**
 * @desc Contenedor principal
 */
export const Location = styled.address`
	color:white;
	font-family:'IBM Plex Sans', sans-serif;
	font-size:1.5rem;
	font-style: normal;
	font-weight:bold;
	text-shadow:1px 1px 1px rgba(0,0,0,0.5);
	margin:2vh;
	@media (max-width: 425px) {
        &{
			font-size:2.5rem;
			margin:1vh ;
        }
    }  
`;
/**
 * @desc Contenedor principal
 */
export const Time = styled.time`
	display:block;
	color:white;
	width:100%;
	font-family:'IBM Plex Sans', sans-serif;
	font-size:1rem;
	text-shadow:1px 1px 1px rgba(0,0,0,0.5);
	text-align:center;
	margin:5px 0px;
	@media (max-width: 425px) {
        &{
			font-size:0.7rem;
        }
    }  
`;

/**
 * @desc Contenedor principal
 */
export const Temp = styled.h1`
	color:white;
	font-family:'IBM Plex Sans', sans-serif;
	font-size:3rem;
	line-height:3rem;
	margin:0px;
	text-shadow:1px 1px 3px rgba(0,0,0,0.5);
`;


/**
 * @desc Contenedor principal
 */
export const Content = styled.section`
	background:white;
	width:-webkit-calc(95% - 20px);
	position:absolute;
	top:75%;
	left:2.5%;
	border-radius:4px;
	padding:15px;
	box-shadow:2px 1px 4px -1px rgba(0,0,0,0.5);
	
	@media (max-width: 425px) {
        &{
			padding:10px;
        }
    }  
`;


/**
 * @desc Contenedor principal
 */
export const Current = styled.h3`
	color:white;
	width: 100%;
	font-family:'IBM Plex Sans', sans-serif;
	text-align:center;
	text-shadow:1px 1px 3px rgba(0,0,0,0.5);
	margin:0;
	margin-top:2vh;
	text-transform:capitalize;
`;

