
/**
 * @desc Dependencias
 */
import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { createBrowserHistory } from 'history';

/**
 * @desc Contenedor.
 */
const App = React.lazy(() => import("./Containers/App/App"));

/**
 * @desc Componente.
 */
import Loading from "./Components/Loading";

/**
 * @desc Api de navegación.
 */
const History = createBrowserHistory();

/**
 * @desc Renderiza la aplicación.
 */
ReactDOM.render(
    <BrowserRouter>
        <Suspense fallback={ Loading }>
            <App history={ History } />
        </Suspense>
    </BrowserRouter>
, document.getElementById('root'));
