/**
 * @desc Configuración
 */
import { OpenWeatherApi } from "../Config/Constants.js";

/**
 * @desc Modelo
 */
class WeatherModel {

    /**
     * @desc Solicita datos del clima actual por una ciudad.
     * 
     * @param { String } city
     * 
     * @return { Promise<Boolean> }
     */
    static getCurrentWeather( city ){

        return new Promise(( resolve, reject ) => {

            try{

                // Solicitud
                fetch(`https://api.openweathermap.org/data/2.5/weather?q=${ city }&lang=es&appid=${ OpenWeatherApi.apiKey}` )
                    .then( response => resolve( response.json() ) )
                    .catch( reject );

                return true;

            }catch( error ){

                // Logueamos el error
                console.log( error );

                return false;

            }

        });

    }

    /**
     * @desc Obtenemos el pronostico del tiempo de los proximos dias.
     * 
     * @param { String } city
     * 
     * @return { Promise<Boolean> }
     */
    static getForecast( city ){

        return new Promise(( resolve, reject ) => {

            try{

                // Solicitud
                fetch(`https://api.openweathermap.org/data/2.5/forecast?q=${ city }&lang=es&appid=${ OpenWeatherApi.apiKey }` )
                    .then( response => resolve( response.json() ) )
                    .catch( reject );

                return true;

            }catch( error ){

                // Logueamos el error
                console.log( error );

                return false;

            }

        });

    }


    /**
     * @desc Obtenemos el pronostico de las ciudades cercanas.
     * 
     * @param { Number } lat
     * @param { Number } lng
     * @param { Number } cnt
     * 
     * @return { Promise<Boolean> }
     */
    static getWeatherOfCitysNearby( lat, lng, cnt = 15 ){

        return new Promise(( resolve, reject ) => {

            try{

                // Solicitud
                fetch(`https://api.openweathermap.org/data/2.5/find?lat=${ lat }&lon=${ lng }&cnt=${ cnt }&appid=${ OpenWeatherApi.apiKey }` )
                    .then( response => resolve( response.json() ) )
                    .catch( reject );

                return true;

            }catch( error ){

                // Logueamos el error
                console.log( error );

                return false;

            }

        });

    }

}

export default WeatherModel;